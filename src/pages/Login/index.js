import React from 'react';

import {Layout, Form, Input, Button, Radio} from 'antd';
import s from './Login.module.scss'
import {withFirebase} from '../../context/firebaseContext.js'

const {Content} = Layout;


class LoginPage extends React.Component {

    state = {
        method: 'signIn'
    }

    onChange = (value) => {
        this.setState({
            method: value.target.value
        })
    }

    onFinish = ({username, password}) => {
        console.log(username, password)
        // const { signInWithEmailAndPassword, createUserWithEmailAndPassword } = this.props.firebase
        switch (this.state.method) {
            case "signIn": this.props.signInWithEmailAndPassword(username, password).then(res => {
                localStorage.setItem('user', res.user.uid)
                this.props.history.push('/')
            }); return
            case "signUp": this.props.createUserWithEmailAndPassword(username, password).then(()=>this.setState({method: 'signIn'}))
            ; return
            default: console.log('something went wrong'); return
        }
    }

    onFinishFailed = (error) => {
        console.log(error)
    }

    layout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };
    tailLayout = {
        wrapperCol: {offset: 8, span: 16},
    };

        render() {
            return <Layout>
                <Content>
                    <div className={s.root}>
                        <Form
                            name="basic"
                            initialValues={{remember: true}}
                            onFinish={this.onFinish}
                            onFinishFailed={this.onFinishFailed}
                        >
                                <Radio.Group defaultValue="signIn" buttonStyle="solid" onChange={this.onChange}>
                                    <Radio.Button value="signIn">Sign In</Radio.Button>
                                    <Radio.Button value="signUp">Sign Up</Radio.Button>
                                </Radio.Group>

                            <Form.Item
                                label="Username"
                                name="username"
                                rules={[{required: true, message: 'Please input your username!'}]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{required: true, message: 'Please input your password!'}]}
                            >
                                <Input.Password/>
                            </Form.Item>

                            <Form.Item {...this.tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Submit
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </Content>
            </Layout>
        }
}
// LoginPage.contextType = FirebaseContext
export default withFirebase(LoginPage);