import React from 'react';
import { Component } from 'react';
import s from './Home.module.scss';
import HeaderBlock from "../../components/HeaderBlock";
import Header from "../..//components/Header";
import Paragraph from '../..//components/Paragraph'
import Card from "../..//components/Card";
import {Input, Spin} from 'antd';
import getTranslation from '../..//services/yandex-dictionary.js'
import FirebaseContext from "../../context/firebaseContext";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../../actions'
import {cardAction, cardRejectedAction, cardResolvedAction} from "../../actions/cardAction";

const { Search } = Input

class HomePage extends Component {
    // state = {
    //     wordArr: [],
    //     rus: '',
    //     eng: '',
    //     isLoading: false
    // }

    // urlRequest = `/cards/${this.props.user.uid}`

    componentDidMount() {
        const { getUserCardsRef } = this.context
        const {
            fetchCards,
            fetchCardsResolved,
            fetchCardsRejected
        } = this.props

        fetchCards( )
        getUserCardsRef().once('value').then(result => {
            fetchCardsResolved(result.val())
            // this.setState({ wordArr: result.val() || []})
        }).catch(err => fetchCardsRejected(err))
        console.log(this.props)
    }
    handleDeletedItem = async (id) => {
        await this.setState(({wordArr}) => {
            const newWordArr = wordArr.filter(item => {
               if (item.id !== id) return true
                return false
            })
            return {
                wordArr: newWordArr
                // wordArr: wordArr
            }
        })
        const { getUserCardsRef } = this.context
        await getUserCardsRef().set(this.state.wordArr)
    }

    // handleInputChange = (event) => {
    //     this.setState({
    //         value: event.target
    //     })
    // }

    handleEngChange = (event) => {
        this.setState({
            eng: event.target.value
        })
    }

    handleRusChange = (event) => {
        this.setState({
            rus: event.target.value
        })
    }

    getWord = async () => {
        if (this.state.eng === '') {
            this.setState(()=>{
                return {
                    rus:'',
                    isLoading:false
                }
            })
            return
        }
        const getWord = await getTranslation(this.state.eng).catch(()=>{return 'Слово не найдено, пожалуйста, введите свой перевод'})
        // wordArr.push({eng:eng, rus:rus, id: wordArr.length+1})
        const newWordArr = [
            ...this.state.wordArr,
            {
                eng:this.state.eng.toLowerCase(),
                rus:this.state.rus.toLowerCase() || getWord.toLowerCase(),
                id:(this.state.wordArr[this.state.wordArr.length-1]) ? this.state.wordArr[this.state.wordArr.length-1].id+1 : 0
            }

        ]
        const { getUserCardsRef } = this.context
        await getUserCardsRef().set(newWordArr)
        this.setState(()=>{
            return {
                wordArr: newWordArr,
                rus:'',
                eng:'',
                isLoading:false
            }
        })
    }

    handleSubmitForm = async (event) => {
        // event.preventDefault()
        this.setState({
            isLoading:true
        }, this.getWord)

        // this.setState(({value})=>{
        //    return {label: value}
        // })
    }

    render() {
        // const {wordArr} = this.state
        const { items, isBusy } = this.props

        if (isBusy) {
            return (
                <div className='loaderClass'>
                    <Spin/>
                </div>
            )
        }

        return (
            <>
                <HeaderBlock>
                    <Header>
                        Время учить слова онлайн
                    </Header>
                    <Paragraph>
                        Используйте что-то там для запоминания
                    </Paragraph>
                </HeaderBlock>
                <form className={s.formComponent}
                    // onSubmit={this.handleSubmitForm}
                >
                    {/*<input*/}
                    {/*    type="text"*/}
                    {/*    value={this.state.eng}*/}
                    {/*    onChange={this.handleEngChange}*/}
                    {/*/>*/}
                    {/*<input*/}
                    {/*    type="text"*/}
                    {/*    value={this.state.rus}*/}
                    {/*    onChange={this.handleRusChange}*/}
                    {/*/>*/}
                    {/*<button className="submitButton">*/}
                    {/*    Add New Word*/}
                    {/*</button>*/}

                    <Input
                        className={s.inputClass}
                        placeholder="English Word"
                        value={this.state.eng}
                        onChange={this.handleEngChange}
                        onPressEnter={this.handleSubmitForm}
                    />
                    <Search
                        className={s.inputClass}
                        placeholder="Слово на русском (свой перевод)"
                        value={this.state.rus}
                        onChange={this.handleRusChange}
                        onSearch={this.handleSubmitForm}
                        loading={this.state.isLoading}
                    />
                </form>
                <div className={s.cardContainer}>
                    {
                        items.map(({eng, rus, id}) => <Card onDeleted={this.handleDeletedItem} key={id} id={id} eng={eng.toLowerCase()} rus={rus.toLowerCase()} />)
                    }
                </div>
                <HeaderBlock
                    hideBackground
                >
                    <Header>
                        Нам нравится это
                    </Header>
                </HeaderBlock>
            </>
        )
    }
}

// const App = () => {
//   return (
//       <>
//         {/*<AppHeader/>*/}
//         {/*<AppList/>*/}
//         <HeaderBlock>
//             <Header>
//                 Время учить слова онлайн
//             </Header>
//             <Paragraph>
//                 Используйте что-то там для запоминания
//             </Paragraph>
//         </HeaderBlock>
//           <div className='cardContainer'>
//               {
//                   wordsList.map(({eng, rus}, index) => <Card key={index} eng={eng} rus={rus}/>)
//               }
//           </div>
//         <HeaderBlock
//             hideBackground
//         >
//             <Header>
//                 Нам нравится это
//             </Header>
//         </HeaderBlock>
//       </>
//   )
// }

HomePage.contextType = FirebaseContext

const mapStateToProps = (state) => {
    return {
        isBusy: state.cards.isBusy,
        items: state.cards.payload || [],
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchCards: cardAction,
        fetchCardsResolved: cardResolvedAction,
        fetchCardsRejected: cardRejectedAction,
        actions
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
