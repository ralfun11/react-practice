import React from 'react';
import { Component } from 'react';
import './App.css';
import HomePage from './pages/Home/';
import LoginPage from './pages/Login';
import { Spin } from 'antd'
import FirebaseContext from "./context/firebaseContext";
import {BrowserRouter, Redirect, Route} from 'react-router-dom'
// import PrivateRoute from './utils/privateRoute'
import { connect } from 'react-redux'
import {bindActionCreators} from "redux";
import {addUserAction} from "./actions/userAction";
// import {Link} from 'react-router-dom'

class App extends Component {

    componentDidMount() {
        const { auth, setUserUid } = this.context
        const { addUser } = this.props
        auth.onAuthStateChanged(user => {
            console.log('####: ' + user)
            if (user) {
                setUserUid(user.uid)
                localStorage.setItem('user', JSON.stringify(user.uid))
                addUser(user)
                this.setState({user})
            } else {
                setUserUid(null)
                localStorage.removeItem('user')
                this.setState({user: false})
            }
        })
    }

    render() {
        return (
            <BrowserRouter>
                {
                    localStorage.getItem('user')
                    ? <Redirect to='/home'/>
                    : <Redirect to='/login'/>
                }
                <Route path='/' exact component={HomePage}/>
                <Route path='/home' component={HomePage}/>
                <Route   path='/login' component={LoginPage}/>
                {/*<Route render={()=><h1>Something went wrong</h1>}/>*/}
            </BrowserRouter>
            // <>
            //     {this.state.user ? <HomePage user={this.state.user} />  : <LoginPage/>}
            //     </>
        )
    }
}
App.contextType = FirebaseContext

const mapStateToProps = (state) => {
    return {
        userUid: state.user.uid
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addUser: addUserAction,
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
