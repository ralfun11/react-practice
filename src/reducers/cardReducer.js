import {FETCH_CARDS, FETCH_CARDS_REJECT, FETCH_CARDS_RESOLVED} from "../actions/actionTypes";

const cardReducer = (state, action) => {
    switch (action.type) {
        case FETCH_CARDS: return {
            payload: [],
            err : null,
            isBusy: true
        }
        case FETCH_CARDS_RESOLVED: return {
            payload: action.payload,
            err : null,
            isBusy: false
        }
        case FETCH_CARDS_REJECT: return {
            payload: null,
            err : action.err,
            isBusy: false
        }
        default: return {
            payload: null,
            err : null,
            isBusy: false
        }
    }
}

export default cardReducer