import counterReducer from './counter.js'
import userReducer from './userReducer.js'
import cardReducer from './cardReducer.js'
import { combineReducers } from 'redux'

export default combineReducers({
    user: userReducer,
    counter: counterReducer,
    cards: cardReducer
})