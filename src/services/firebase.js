import * as firebase from "firebase/app";
import 'firebase/database'
import 'firebase/auth'

class Firebase {
    constructor() {
        firebase.initializeApp({
            apiKey: process.env.REACT_APP_API_KEY,
            authToken: process.env.REACT_APP_AUTH_DOMAIN,
            databaseURL: process.env.REACT_APP_DATABASE_URL,
            projectId: process.env.REACT_APP_PROJECT_ID,
            storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
            messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
            appId: process.env.REACT_APP_APP_ID
        })

        this.auth = firebase.auth()
        this.database = firebase.database()

        this.userUid = null;
    }

    setUserUid = (uid) => this.userUid = uid

    signInWithEmailAndPassword = (email, password) => this.auth.signInWithEmailAndPassword(email, password)
    createUserWithEmailAndPassword = (email, password) => this.auth.createUserWithEmailAndPassword(email, password)

    getUserCardsRef = () => this.database.ref(`/cards/${this.userUid}`)
}

export default Firebase
