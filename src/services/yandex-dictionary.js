const getTranslation = async (text, lang = "en-ru") => {
    const key = process.env.REACT_APP_YANDEX_API_KEY

    const json = await fetch(`https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=${key}&lang=${lang}&text=${text}`);
    const translationObject = await json.json();
    console.log(translationObject.def[0].tr[0].text);
    return translationObject.def[0].tr[0].text|| []
}

export default getTranslation