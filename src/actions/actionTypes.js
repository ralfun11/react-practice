export const ADD_USER = 'ADD_USER';
export const PLUS = 'PLUS';
export const MINUS = 'MINUS';

export const FETCH_CARDS = 'FETCH_CARDS';
export const FETCH_CARDS_RESOLVED = 'FETCH_CARDS_RESOLVED';
export const FETCH_CARDS_REJECT = 'FETCH_CARDS_REJECT';