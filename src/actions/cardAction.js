import {FETCH_CARDS, FETCH_CARDS_REJECT, FETCH_CARDS_RESOLVED} from "./actionTypes";

export const cardAction = () => ({
    type: FETCH_CARDS
})

export const cardResolvedAction = (payload) => ({
    type: FETCH_CARDS_RESOLVED,
    payload
})

export const cardRejectedAction = (err) => ({
    type: FETCH_CARDS_REJECT,
    err
})