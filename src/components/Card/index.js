import React, {Component} from 'react';
import s from './Card.module.scss'
import cl from 'classnames';
import {CheckSquareOutlined, DeleteOutlined} from '@ant-design/icons'

class Card extends Component {

    state = {
        done: false,
        isRemembered: false
    }

    handleClick = () => {
        this.setState({
            done: !this.state.done
        })
    }
    handleRemembered = () => {
        this.setState(
            (state) => {
                return {
                    isRemembered: !state.isRemembered
                }
            }
        )
    }
    handleDelete = () => {
        this.props.onDeleted(this.props.id)
    }

    render() {
        const {eng, rus} = this.props
        const {done, isRemembered} = this.state


        return (
    <div className={s.root}>
        <div className={s.cardContainer}>
            <div
                className={ cl(s.card, {
                    [s.done]: done,
                    [s.isRemembered]: isRemembered
                }) }
                onClick={this.handleClick}
            >
                <div className={s.cardInner}>
                    <div className={s.cardFront}>
                        {eng}
                    </div>
                    <div className={s.cardBack}>
                        {rus}
                    </div>
                </div>
            </div>
        </div>
        <div className={s.buttonGroup}>
            <div className={ cl( s.crossIcon, s.icons)}>
                <CheckSquareOutlined onClick={this.handleRemembered}/>
            </div>
            <div className={ cl( s.deleteIcon, s.icons)}>
                <DeleteOutlined onClick={this.handleDelete}/>
            </div>
        </div>
    </div>
        )
    }
}

export default Card;