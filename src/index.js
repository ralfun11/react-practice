import React from 'react';
import ReactDom from 'react-dom';

import './index.css';
import 'antd/dist/antd.css'

import App from './App.js';

import FirebaseContext from './context/firebaseContext.js'
import Firebase from './services/firebase'

import {applyMiddleware, createStore} from 'redux'
import rootReducers from './reducers'
import {Provider} from 'react-redux'
import {logger} from 'redux-logger'

const store = new createStore(rootReducers, applyMiddleware(logger))

ReactDom.render(
    <Provider store={store}>
        <FirebaseContext.Provider value={new Firebase()}>
            <App/>
        </FirebaseContext.Provider>
    </Provider>, document.getElementById('root')
)